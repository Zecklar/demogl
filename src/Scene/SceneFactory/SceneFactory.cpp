#include "SceneFactory.hpp"
#include "Scene\Scenes\TestScene.hpp"

SceneFactory::SceneFactory()
{
}

SceneFactory::~SceneFactory()
{
}

std::shared_ptr< IScene > SceneFactory::createScene(Scene sceneName, std::shared_ptr< ICamera >camera) {
	switch (sceneName) {
	case TEST:
		return std::make_shared< TestScene >("Test", camera);
	default:
		return nullptr;
	};
}
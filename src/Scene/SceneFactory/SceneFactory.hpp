#ifndef SCENEFACTORY
#define SCENEFACTORY

#include <memory>
#include "Scene\Scenes\Scenes.hpp"
#include "Scene/Scenes/IScene.hpp"

class SceneFactory
{
public:
	SceneFactory();
	~SceneFactory();

	std::shared_ptr< IScene >createScene(Scene sceneName, std::shared_ptr< ICamera >camera);
};

#endif
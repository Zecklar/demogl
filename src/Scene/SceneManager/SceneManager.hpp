#pragma once

#include <memory>
#include <map>
#include "Scene/Scenes/IScene.hpp"

class SceneManager
{
public:
	SceneManager();
	~SceneManager();

	void addScene(std::shared_ptr< IScene >scene);

	// ---

	void render();
	
private:

	// All the scenes
	std::map<std::string, std::shared_ptr<IScene> >m_scenes;

	// Ongoing scene
	std::shared_ptr< IScene >m_currentScene;
};


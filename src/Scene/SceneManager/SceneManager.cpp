#include "SceneManager.hpp"

SceneManager::SceneManager()
{
}


SceneManager::~SceneManager()
{
}

void SceneManager::render() {
	m_currentScene->render();
}


void SceneManager::addScene(std::shared_ptr< IScene >scene) {

	// Not null and not already in the map
	if (scene && m_scenes.find(scene->getName()) == m_scenes.end()) {
		m_scenes[scene->getName()] = scene;
	}

}
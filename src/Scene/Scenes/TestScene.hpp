#ifndef TESTSCENE
#define TESTSCENE

#include "Scene/Scenes/IScene.hpp"

class TestScene : public IScene
{
public:
	TestScene(std::string name, std::shared_ptr< ICamera >camera);
	~TestScene();

	void init();
	void render();
};

#endif
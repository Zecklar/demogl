#pragma once

#include <memory>
#include <string>
#include "Camera\ICamera.hpp"
#include "Common/Logger/Logger.hpp"

class IScene
{
public:
	IScene(std::string name, std::shared_ptr< ICamera >camera);
	virtual ~IScene(){};

	virtual void init() = 0;
	virtual void render() = 0;

	void setCamera(std::shared_ptr< ICamera >camera);
	std::string getName() const;
	
private:
	std::string m_name;
	std::shared_ptr< ICamera >m_camera;
};
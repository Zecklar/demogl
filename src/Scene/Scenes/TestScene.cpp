#include "TestScene.hpp"


TestScene::TestScene(std::string name, std::shared_ptr< ICamera >camera):
IScene(name, camera)
{
}

TestScene::~TestScene()
{
}

void TestScene::init() {
	//TODO: Shader manager
	//TODO: Meshes
	//TODO: Camera trajectory (moving and aiming)
	//TODO: Heightmap
	//TODO: Skybox
	//TODO: Textures
	//TODO: Light
	Logger::printMessage(INFO, "TestScene initialized");
}

void TestScene::render() {

}



#include "IScene.hpp"

IScene::IScene(std::string name, std::shared_ptr< ICamera >camera) :
m_name(name),
m_camera(camera)
{
}

std::string IScene::getName() const {
	return m_name;
}

void IScene::setCamera(std::shared_ptr< ICamera >camera) {
	m_camera = camera;
}
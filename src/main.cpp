#include <iostream>
#include "System/System.hpp"

int main() {

	System system;
	status init = system.init();

	if (init == OK) {
		system.run();
	}
	else {
		std::cerr << "Couldn't initialize the program: " << init << std::endl;
	}

	return 0;
}
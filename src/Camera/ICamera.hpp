#ifndef ICAMERA
#define ICAMERA

#include <memory>
#include <glm\glm.hpp>
#include "InputManager\InputManager.hpp"

class ICamera
{
public:
	ICamera();
	~ICamera();

protected:

	// Current position
	glm::vec3 m_pos;
	
	// Direction where to aim
	glm::vec3 m_view;

	// Camera's up-vector
	glm::vec3 m_up;

	float m_speed;
	

	std::shared_ptr< InputManager >m_inputManager;
};

#endif
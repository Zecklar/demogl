#include <utility>
#include <glm\gtx\rotate_vector.hpp>
#include "Camera.hpp"
#include "Common/wininfo.hpp"

Camera::Camera()
{
}


Camera::~Camera()
{
}

void Camera::update() {
	
}

void Camera::move() {

	// TODO: Move to FPS Timer class
	float frametime = (float)glfwGetTime();
	glfwSetTime(0);

	// View direction
	glm::vec3 view_dir = m_view - m_pos;
	view_dir = glm::normalize(view_dir);
	view_dir *= m_speed;

	// Get side direction
	glm::vec3 strafe_dir = glm::cross(m_view - m_pos, m_up);
	strafe_dir = glm::normalize(strafe_dir);
	strafe_dir *= m_speed;

	//Add movement
	glm::vec3 move;

	if (m_inputManager->keyPressed(K_FORWARD)) move += frametime * view_dir;
	if (m_inputManager->keyPressed(K_BACKWARD)) move -= frametime * view_dir;
	if (m_inputManager->keyPressed(K_RIGHT)) move += frametime * strafe_dir;
	if (m_inputManager->keyPressed(K_LEFT)) move -= frametime * strafe_dir;
	
	//Add the result to vectors
	m_pos += move; 
	m_view += move;
}

void Camera::rotate() {
	//Mouse coordinates
	std::pair< double, double >mouseCoords = m_inputManager->getCursorPos();

	// Sensitivity
	float sensitivity = m_inputManager->getSensitivity();
	
	//Calculate delta move
	float deltax = (float)(WINSCREENWIDTH / 2 - mouseCoords.first) * sensitivity;
	float deltay = (float)(WINSCREENHEIGHT / 2 - mouseCoords.second) * sensitivity;

	//Rotate X
	if (deltax != 0.0f)
	{
		m_view -= m_pos;
		m_view = glm::rotate(m_view, deltax, glm::vec3(0.0f, 1.0f, 0.0f));
		m_view += m_pos;
	}

	//Rotate Y
	if (deltay != 0.0f)
	{
		glm::vec3 axis = glm::normalize(glm::cross(m_view - m_pos, m_up));

		float angle = deltay + getAngleX();

		if (angle > -89.90f && angle < 89.90f)
		{
			m_view -= m_pos;
			m_view = glm::rotate(m_view, deltay, axis);
			m_view += m_pos;
		}
	}

	//Set cursor back to screen center
	m_inputManager->centerCursorPos();
}

// Calculate the x-axis angle
float Camera::getAngleX() {
	glm::vec3 vDir = m_view - m_pos;
	vDir = glm::normalize(vDir);
	glm::vec3 vDir2 = vDir;

	vDir2.y = 0.0f;
	vDir2 = glm::normalize(vDir2);
	float fAngle = acos(glm::dot(vDir2, vDir))*(180.0f / glm::pi< float >());

	if (vDir.y < 0)
	{
		fAngle *= -1.0f;
	}

	return fAngle;
}
#pragma once

#include <GLFW\glfw3.h>
#include "Camera\ICamera.hpp"

class Camera: public ICamera
{
public:
	Camera();
	~Camera();

	void update();

private:
	float getAngleX();
	void move();
	void rotate();
};


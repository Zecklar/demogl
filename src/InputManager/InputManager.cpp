#include "InputManager.hpp"
#include "Common/wininfo.hpp"

InputManager::InputManager(GLFWwindow* window):
m_window(window)
{
}


InputManager::~InputManager()
{
}


bool InputManager::keyPressed(keyNum_t key) {
	return (glfwGetKey(m_window, key) == GLFW_PRESS);
}

bool InputManager::keyReleased(keyNum_t key) {
	return (glfwGetKey(m_window, key) == GLFW_RELEASE);
}

bool InputManager::keyDown(keyNum_t key) {
	return (glfwGetKey(m_window, key) == GLFW_REPEAT);
}

std::pair< double, double > InputManager::getCursorPos() {
	double xpos, ypos = 0;
	glfwGetCursorPos(m_window, &xpos, &ypos);

	return std::pair< double, double>(xpos, ypos);
}

float InputManager::getSensitivity() {
	return m_sensitivity;
}

void InputManager::setCursorPos(std::pair< double, double >mousePos) {
	glfwSetCursorPos(m_window, mousePos.first, mousePos.second);
}

void InputManager::centerCursorPos() {
	glfwSetCursorPos(m_window, WINSCREENWIDTH / 2, WINSCREENHEIGHT / 2);
}
#pragma once

#include <GLFW\glfw3.h>
#include <utility>
#include <Common\controls.hpp>

class InputManager
{
public:
	InputManager(GLFWwindow* window);
	~InputManager();

	// Key methods
	bool keyPressed(keyNum_t key);
	bool keyReleased(keyNum_t key);
	bool keyDown(keyNum_t key);

	// Cursor methods
	std::pair< double, double >getCursorPos();
	void setCursorPos(std::pair< double, double >mousePos);
	void InputManager::centerCursorPos();
	float getSensitivity();

private:
	GLFWwindow* m_window;
	float m_sensitivity;
};


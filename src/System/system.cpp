#include <iostream>
#include "System.hpp"
#include "Common/wininfo.hpp"
#include "Common/errorhandling.hpp"
#include "Common/Logger/Logger.hpp"
#include "Scene/SceneFactory/SceneFactory.hpp"

System::System()
{
}


System::~System()
{
	glfwTerminate();
}

status System::init() {

	// Init the GLFW lib
	status glfw = initGLFW();

	if (glfw != OK) {
		return glfw;
	}

	// ---

	status glew = initGLEW();

	if (glew != OK) {
		return glew;
	}

	// ---

	// Camera
	initCamera();

	// Scene stuff
	initSceneManager();
	status sceneInit = initScenes();

	if (sceneInit != OK) {
		return sceneInit;
	}

	// ---

	Logger::printMessage(INFO, "Initialization done");

	return OK;
}

void System::run() {
	Logger::printMessage(INFO, "Opening window...");

	while (glfwGetKey(m_window, GLFW_KEY_ESCAPE) != GLFW_PRESS && !glfwWindowShouldClose(m_window)) {

		m_sceneManager->render();

		glfwPollEvents();
	}
}

status System::initGLFW() {
	Logger::printMessage(INFO, "Initializing GLFW: ");

	// Init lib
	if (!glfwInit()) {
		Logger::printMessage(ERROR, "GLFW Initialization failed");
		return GLFW_INIT_ERROR;
	}

	// Set the OpenGL version (4.3)
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);

	m_window = glfwCreateWindow(WINSCREENWIDTH, WINSCREENHEIGHT, WINTITLE, NULL, NULL);

	// Set the context to the OpenGL version set above
	glfwMakeContextCurrent(m_window);

	// Hide cursor
	glfwSetInputMode(m_window, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);

	// Set callback function for error printing
	glfwSetErrorCallback(errorMsg);

	Logger::printMessage(INFO, "GLFW initialized");
	return OK;
}

status System::initGLEW() {
	// Required by OpenGL version 4.3
	glewExperimental = GL_TRUE;

	GLenum reason = glewInit();

	if (reason != GLEW_OK){
		std::cout << "ERROR " << glewGetErrorString(reason) << std::endl;
		return GLEW_INIT_ERROR;
	}

	Logger::printMessage(INFO, "GLEW initialized");
	return OK;
}

status System::initSceneManager() {
	m_sceneManager = std::make_unique< SceneManager >();

	if (m_sceneManager) {
		initScenes();
		return OK;
	}
	else {
		Logger::printMessage(ERROR, "SceneManager not initialized properly");
		return SCENEMANAGER_INIT_ERROR;
	}
}

void System::initCamera() {
	m_camera = std::make_shared< Camera >();
}

status System::initScenes() {
	SceneFactory factory;

	std::shared_ptr< IScene >testScene = factory.createScene(Scene::TEST, m_camera);
	m_sceneManager->addScene(testScene);

	return OK;
}
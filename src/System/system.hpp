#pragma once

#include <memory>
#include <GL\glew.h>
#include <GLFW\glfw3.h>
#include "Common/statuscode.hpp"
#include "Scene/SceneManager/SceneManager.hpp"
#include "Camera/Camera.hpp"

class System
{
public:
	System();
	~System();

	status init();
	void run();

private:
	std::shared_ptr< Camera >m_camera;
	std::unique_ptr< SceneManager >m_sceneManager;
	GLFWwindow* m_window;

	status initGLFW();
	status initGLEW();

	status initSceneManager();
	status initScenes();

	void initCamera();
};


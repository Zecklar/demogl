#ifndef WININFO
#define WININFO

// All the window related properties
const unsigned int WINSCREENWIDTH = 1024;
const unsigned int WINSCREENHEIGHT = 720;
const char* const WINTITLE = "Terrain OpenGL";

#endif
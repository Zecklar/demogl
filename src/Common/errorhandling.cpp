#include <iostream>
#include "errorhandling.hpp"

void errorMsg(int error, const char* description) {
	std::cout << "GLFW ERROR OCCURED!" << std::endl;
	std::cout << "Error code: " << error << " Description: " << description << std::endl;
}
#include <array>
#include <utility>
#include <sstream>
#include <fstream>
#include <vector>
#include "ShaderTool.hpp"
#include "Common/Logger/Logger.hpp"

typedef std::pair< std::string, GLenum > ShaderType;

GLuint ShaderTool::loadShader(std::string shaderFilePath) {

	// Define which shader will be created
	GLenum shaderType = defineShaderType(shaderFilePath);

	// The actual shader object
	GLuint shaderID = glCreateShader(shaderType);

	// The shader source file
	std::ifstream shaderFile(shaderFilePath);

	if (!shaderFile.is_open()) {
		Logger::printMessage(MessageLevel::ERROR, "Couldn't open shader file: " + shaderFilePath);

		// glCreateShader returns 0 if error has occured
		return 0;
	}

	// Read the source file
	std::string shaderCode = "";
	std::string line = "";
	while (std::getline(shaderFile, line)) {
		shaderCode += "\n" + line;
	}

	shaderFile.close();

	// Set the read source code to the shader object
	char const* source_ptr = shaderCode.c_str();
	glShaderSource(shaderID, 1, &source_ptr, NULL);

	Logger::printMessage(MessageLevel::INFO, "Attached source code from " + shaderFilePath +
		" to shader ID " + std::to_string(shaderID));

	return shaderID;
}

// Compiles shader where the source is already attached
bool ShaderTool::compileShader(GLuint shaderID) {
	GLint compilationStatus = GL_FALSE;

	// Compile
	glCompileShader(shaderID);

	// Read compilation status
	glGetShaderiv(shaderID, GL_COMPILE_STATUS, &compilationStatus);

	// Error occured while compiling, print the error log
	if (!compilationStatus) {
		printCompileError(shaderID);
		return false;
	}

	return true;
}

// Check the shader type defined by the file extension
GLenum ShaderTool::defineShaderType(std::string shaderName) {

	// Valid file extensions
	std::array< std::pair<std::string, GLenum>, 3 >fileExtensions = {
		ShaderType(".vert", GL_VERTEX_SHADER),
		ShaderType(".frag", GL_FRAGMENT_SHADER),
		ShaderType(".geo", GL_GEOMETRY_SHADER)
	};

	for (ShaderType t : fileExtensions) {
		if (shaderName.find(t.first) != std::string::npos) {
			return t.second;
		}
	}

	// In case of invalid file extension
	return -1;
}

// Prints compilation error log
void ShaderTool::printCompileError(GLuint shaderID) {
	int logLength = 0;

	// Get the log length and allocate enough space for that
	glGetShaderiv(shaderID, GL_INFO_LOG_LENGTH, &logLength);
	std::vector< GLchar >errorlog(logLength + 1);

	// Read the actual content of infolog
	glGetShaderInfoLog(shaderID, logLength, NULL, &errorlog[0]);

	std::string strErrorLog = std::string(errorlog.data());
	Logger::printMessage(MessageLevel::ERROR, strErrorLog);
}
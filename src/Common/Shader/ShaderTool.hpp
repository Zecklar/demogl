#ifndef SHADERTOOL_HPP
#define SHADERTOOL_HPP

#include  <GL/glew.h>
#include  <string>

class ShaderTool
{
public:

	static GLuint loadShader(std::string shaderName);
	static bool compileShader(GLuint shaderID);

private:

	static GLenum defineShaderType(std::string shaderName);
	static void printCompileError(GLuint shaderID);
};

#endif

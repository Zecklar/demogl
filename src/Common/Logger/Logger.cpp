#include <iostream>
#include "Logger.hpp"


Logger::Logger()
{
}


Logger::~Logger()
{
}

void Logger::printMessage(MessageLevel lvl, std::string msg) {
	switch (lvl) {
	case INFO:
		std::cout << "[INFO] ";
		break;
	case ERROR:
		std::cout << "[ERROR] ";
		break;
	default:
		break;
	};

	std::cout << msg << std::endl;
}
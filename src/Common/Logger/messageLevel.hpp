#ifndef MESSAGELEVEL
#define MESSAGELEVEL

enum MessageLevel {
	INFO,
	WARNING,
	ERROR
};

#endif
#ifndef LOGGER
#define LOGGER

#include <string>
#include "Common/Logger/messageLevel.hpp"

class Logger
{
public:
	Logger();
	~Logger();

	static void printMessage(MessageLevel lvl, std::string msg);
};

#endif
#pragma once

// Int values for all the used keys
typedef enum {
	K_FORWARD = 'W',
	K_BACKWARD = 'S',
	K_LEFT = 'A',
	K_RIGHT = 'D'
} keyNum_t;
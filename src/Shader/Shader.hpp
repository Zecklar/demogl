#ifndef SHADER_HPP
#define SHADER_HPP

#include <string>
#include <GL/glew.h>

class Shader 
{
public:
	Shader(const std::string sName);
	~Shader();

	bool initShader();

	void activate();
	void deactivate();

private:
	GLhandleARB sProgram;

	// ---

};

#endif

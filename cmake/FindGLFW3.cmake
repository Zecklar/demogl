find_path(GLFW_INCLUDE_DIR
	NAMES
		GLFW/glfw3.h
	PATHS
		"${GLFW_LOCATION}/include"
		"$ENV{GLFW}/include"
)

find_library( GLFW_LIBRARIES
	NAMES
		glfw glfw3
	PATHS
		"${GLFW_LOCATION}/lib"
		"$ENV{GLFW}/lib"
)

if(GLFW_INCLUDE_DIR AND GLFW_LIBRARIES)
	set(GLFW_FOUND "YES")
endif(GLFW_INCLUDE_DIR AND GLFW_LIBRARIES)

if(GLFW_FOUND)
	message(STATUS "Found GLFW: ${GLFW_INCLUDE_DIR}")
else(GLFW_FOUND)
	message(FATAL_ERROR "could NOT find GLFW")
endif(GLFW_FOUND)
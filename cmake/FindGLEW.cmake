find_path(GLEW_INCLUDE_DIR
	NAMES
		GL/glew.h
	PATHS
		"${GLEW_LOCATION}/include"
		"$ENV{GLEW}/include"
)

find_library( GLEW_LIBRARIES
	NAMES
		glew
	PATHS
		"${GLFW_LOCATION}/lib"
		"$ENV{GLFW}/lib/Release/x64"
)

if(GLEW_INCLUDE_DIR AND GLEW_LIBRARIES)
	set(GLEW_FOUND "YES")
endif(GLEW_INCLUDE_DIR AND GLEW_LIBRARIES)

if(GLEW_FOUND)
	message(STATUS "Found GLEW: ${GLEW_INCLUDE_DIR}")
else(GLEW_FOUND)
	message(FATAL_ERROR "could NOT find GLEW")
endif(GLEW_FOUND)